import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DoubleStack {

   public static void main(String[] args) {

      double result = interpret("2 DUP +");
      System.out.println(result);
   }


      private LinkedList<Double> stack;

   private Double rotatedOperand;

   DoubleStack() {
      stack = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clonedStack = new DoubleStack();

      for (double aDouble : stack) {
         clonedStack.stack.addLast(aDouble);
      }
      return clonedStack;
   }

   public boolean stEmpty() {
      return stack.size() == 0;
   }

   public void push (double a) {
      stack.addLast(a);
   }

   public void op (String s){

      if (s.equals("DUP")){
         if (this.stEmpty()){
            throw new RuntimeException("Wrong input: " + s);
         }

         double blah = this.pop();

         this.push(blah);
         this.push(blah);
         return;
      }


      if (stack.size() < 2) {

         throw new RuntimeException("Error during op() running. Wrong input" +
                 " Current size: " + stack.size() + "Input: " + s);
      }
      double operand1 = this.pop();
      double operand2 = this.pop();
      double result;

      if ("+".equals(s)) {
         result = operand2 + operand1;
         this.push(result);
      } else if ("-".equals(s)) {
         result = operand2 - operand1;
         this.push(result);
      } else if ("*".equals(s)) {
         result = operand2 * operand1;
         this.push(result);
      } else if ("/".equals(s)) {
         result = operand2 / operand1;
         this.push(result);
      } else if ("SWAP".equals(s)) {
         this.push(operand1);
         this.push(operand2);

      } else if ("ROT".equals(s)) {
         if (stEmpty()) {
            throw new RuntimeException("Wrong input: " + s + "");
         }

         double operand3 = this.pop();
         this.push(operand2);
         this.push(operand1);
         this.push(operand3);
      }
      else {
         throw new RuntimeException("Error during op() running. Incorrect input. Invalid operation: " + s);
      }
   }

   public double pop() {
      if (stEmpty()){

         throw new RuntimeException("Error during pop() running. There is no element to be deleted.");
      }
      return stack.pollLast();
   }

   public double tos() {
      if (stEmpty()){
         throw new RuntimeException("tos() error. Object has to have at least 1 element to run method tos().");
      }
      return stack.peekLast();
   }

   @Override
   public boolean equals (Object o) {
      if (!(o instanceof DoubleStack)) {
         return false;
      }

      if (((DoubleStack) o).stack.size() != stack.size()){
         return false;
      }

      if (((DoubleStack) o).stEmpty() & stEmpty()) {
         return true;
      }

      for (int i = 0; i < stack.size(); i++) {
          if (!Objects.equals(stack.get(i), ((DoubleStack) o).stack.get(i))){
             return false;
          }

      }
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty())
         return "empty";
      StringBuffer buffer = new StringBuffer();
      for (Double aDouble : stack) {
         buffer.append(String.valueOf(aDouble) + " ");
      }
      return buffer.toString();
   }

   public static double interpret (String pol) {
      /* Piece of code was taken from https://stackoverflow.com/questions/6020384/create-array-of-regex-matches*/

      // Split string to symbol list with no spaces.
      Pattern pattern = Pattern.compile("\\S+");
      List<String> commands = new ArrayList<>();
      Matcher m = pattern.matcher(pol);
      while (m.find()) {
         commands.add(m.group());
      }

      if(commands.size() == 0){
         throw new RuntimeException("interpret() error. Input is missing.");
      }

      // calculation
      DoubleStack rpn = new DoubleStack();
      for (String command : commands) {
            try {
               rpn.push(Double.parseDouble(command));
            }
            catch (Exception e) {
               rpn.op(command);
            }
      }

      double result = rpn.pop();

      if (!rpn.stEmpty()) {
         throw new RuntimeException("Error during interpret(). Wrong input: " + pol + ". Too many operands.");
      }

      return result;
   }
}